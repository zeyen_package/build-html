# buildHtml

#### 介绍
PHP构建常用的Html代码，例如input单选框，select下拉选择框等等。。。，方便构建后台系统使用

#### 安装教程

1.  检测电脑中是否安装composer，如若没有，请参考composer教程安装composer
2.  在项目更目录执行 composer require zeyen0186/build-html，进行安装,或者添加在项目的composer.json文件中，再执行composer update命令

#### 使用说明

1. 生成select Html代码

   ``

   ```
   use Build_Html\Select;
   $list = [
       0=>'Test0',
       1=>'Test1',
       2=>'Test2',
       3=>'Test3',
       4=>'Test4',
   ];
   $str1 = Select::buildSelectBySimple($list,1); //简单模式
   
   
   $list = [
       1=>['id'=>1,'text'=>'Test1'],
       2=>['id'=>2,'text'=>'Test2'],
       3=>['id'=>3,'text'=>'Test3'],
       4=>['id'=>4,'text'=>'Test4'],
   ];
   
   $str2 = Select::buildSelectByDouble($list,'id','text',2); //升级模式
   ```

2. 生成radio Html代码

   ``

   ```
   use Build_Html\Radio;
   .
   .
   .
   .
   .
   
   //业务代码与上类似
   ```



3. 生成checkbox Html代码

   ``

   ```
   use Build_Html\Select;
   .
   .
   .
   .
   .
   //业务代码与上类似
   ```


#### 作者信息

1.  Zeyen  邮箱 zeyen0186@aliyun.com
2.  Zeyen 官方博客 [www.zeyen.cn](https://www.zeyen.cn)
3. 你可以 [https://gitee.com/athenazetan](https://gitee.com/athenazetan) 这个地址来了解 Zeyen的其他优秀开源项目

   

