<?php

namespace Build_Html;
class Select
{
    /**
     * 初始化
     * Select constructor.
     */
    public function __construct()
    {
        
    }

    /**
     * 构建简单一维数组的select选择下拉框代码
     * @author Zeyen
     * @date 2021-05-20   祝自己520快乐 😄
     * @param array $list  一维数组值
     * @param string $selected    需要默认选中的option的value值
     * @return string
     */
    public static function buildSelectBySimple($list=[],$selected='')
    {
      $str = '';
      if($list){
          foreach ($list  as  $key=>$value){
              if($key == $selected){
                  $str .= '<option  selected value="'.$key.'">'.$value.'</option>';
              }else{
                  $str .= '<option  value="'.$key.'">'.$value.'</option>';
              }
          }
      }
      return $str;
    }


    /**
     * 构建复杂二维数组的select选择下拉框代码
     * @author Zeyen
     * @date 2021-05-20   祝自己520快乐 😄
     * @param $list   //二维数组值
     * @param $field_one  //二维数组value值中的第一个字段，对应option的value
     * @param $field_two  //二维数组value值中的第二个字段，对应option的text，用于显示
     * @param string $selected    //需要默认选中的option的value值
     * @return string
     */
    public static function buildSelectByDouble($list,$field_one,$field_two,$selected='')
    {
        $str = '';
        if($list){
            foreach ($list  as  $key=>$value){
                if($value[$field_one] == $selected){
                    $str .= '<option  selected value="'.$value[$field_one].'">'.$value[$field_two].'</option>';
                }else{
                    $str .= '<option  value="'.$value[$field_one].'">'.$value[$field_two].'</option>';
                }
            }
        }
        return $str;
    }

}