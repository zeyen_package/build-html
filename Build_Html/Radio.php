<?php


namespace Build_Html;


class Radio
{
    /**
     * 初始化
     * Select constructor.
     */
    public function __construct()
    {

    }

    /**
     * 构建简单一维数组的radio单选框代码
     * @author Zeyen
     * @date 2021-05-20   祝自己520快乐 😄
     * @param array $list  一维数组值
     * @param string $checked    需要默认选中的value值
     * @return string
     */
    public static function buildRadioBySimple($list=[],$checked='')
    {
        $str = '';
        if($list){
            foreach ($list  as  $key=>$value){
                if($key == $checked){
                    $str .= '<input type="radio" name="radio" checked value="'.$key.'">'.$value.'';
                }else{
                    $str .= '<input type="radio" name="radio"  value="'.$key.'">'.$value.'';
                }
            }
        }
        return $str;

    }

    /**
     * 构建复杂二维数组的radio单选框代码
     * @author Zeyen
     * @date 2021-05-20   祝自己520快乐 😄
     * @param $list   //二维数组值
     * @param $field_one  //二维数组value值中的第一个字段，对应radio的value
     * @param $field_two  //二维数组value值中的第二个字段，对应radio的text，用于显示
     * @param string $checked    //需要默认选中的value值
     * @return string
     */
    public static function buildRadioByDouble($list,$field_one,$field_two,$checked='')
    {
        $str = '';
        if($list){
            foreach ($list  as  $key=>$value){
                if($value[$field_one] == $checked){
                    $str .= '<input type="radio" name="radio" checked value="'.$value[$field_one].'">'.$value[$field_two].'';
                }else{
                    $str .= '<input type="radio" name="radio"  value="'.$value[$field_one].'">'.$value[$field_two].'';
                }
            }
        }
        return $str;
    }

}